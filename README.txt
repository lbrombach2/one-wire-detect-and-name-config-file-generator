Simple c++ program to detect one wire devices like DS18B20 temperature sensors 
and save a file with their device ID's and a user-designated name. The use can then write any number of programs and plug in and unplug devices based on location or for easy replacement without having to hard code your programs with individual device IDs. Just plug your devices into your raaspberry pi and run the program - it will give you a list of all it sees, then just enter a line number to add or change it's name. Don't forget to press 999 to save the file. You could steal the Read() function and modify for your own program that actually uses the sensors.
I arbitrarily set the max number of devices to 20. You could change that easily
as well as the file name and location. I have MYPATH set as 
"/etc/DS18B20TempDevices.conf" and for that to work you'll have to run with sudo.
If you'd prefer not to use sudo you can change the string MYPATH to a
non-protected folder. I've tested a bit, but have some mroe to go when I have time.

As always, please advise me of any bugs you find.

