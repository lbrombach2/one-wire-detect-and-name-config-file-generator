#include <iostream>
//#include <cstdio>
//#include <cstdlib>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <iomanip>


using namespace std;

const int MAX = 20;
const string MYPATH = "/etc/DS18B20TempDevices.conf";
bool Scan();
bool Read();
void Name();
void List();
void Save();
void Delete_Device();

struct tempSensors
{
string name = "I have no name";
char sensorID[16]={' ', '\0'};
bool isOnline = false;
};
tempSensors sensors[MAX];
tempSensors confFileSensors[MAX];
int firstEmpty = 0;


int main()
{

    if(!Scan() ) return 1; //checks for connected devices

    Read();  //reads last saved config file

    Name();

    Save();

  return 0;
}
////////////////////END main()/////////////////////////////////


void List()
{

    cout<<"    "<<"Unique Sensor ID"<<"    "<<" Sensor Name"<<endl;
    for(int i=0; i<MAX; i++)
    {
        cout<<setw(2)<<i<<"  "<<setw(16)<<sensors[i].sensorID<<"     "<<sensors[i].name;
        if(sensors[i].isOnline==false){cout<<"   ***Not Detected***"<<endl;}
        else cout<<endl;
    }

    cout<<endl<<"***Warning - Wait 120 seconds after plugging in/uplugging devices"
        <<"\nfor the W1 directory to update to avoid inconsistent scan results"
        <<"\nand run from command line with sudo or file may not be writabled***"<<endl<<endl;

}
//////////////////////Scan()//////////////////////
bool Scan()
{
 DIR *dir;
 struct dirent *dirent;
 char dev[16];      // Dev ID
 char path[] = "/sys/bus/w1/devices";
 //ssize_t numRead;

 dir = opendir (path);


    if (dir != NULL)
    {
    while ((dirent = readdir (dir)))
   // 1-wire devices are links beginning with 28-
        if (dirent->d_type == DT_LNK
        && strstr(dirent->d_name, "28-") != NULL
        && firstEmpty < MAX  )
        {
        strcpy(dev, dirent->d_name);
        strcpy(sensors[firstEmpty].sensorID,dev);
        sensors[firstEmpty].isOnline=true;
        std::cout<<"\nCount Found Online: "<<firstEmpty<<"   "<<sensors[firstEmpty].sensorID;
	firstEmpty++;
        }

        cout<<"not found"<<endl;
        (void) closedir (dir);
        return true;
    }else
    {
    perror("Couldn't open the w1 devices directory");
    return false;
    }
}
///////////////////////Read()////////////////
bool Read()
{
    tempSensors temp;
    bool matched = false;

    ifstream inFile;
    inFile.open(MYPATH.c_str());
    if(inFile.fail())
    {
    cout<<"DS18B20TempDevices.conf file not found. Please Name your devices: "<<endl;
    system("pause");
    return false;
    }

while(!inFile.eof() && firstEmpty<MAX )
{
matched = false;
inFile.ignore(50, '#');
inFile.ignore(50, '\n');
inFile>>temp.sensorID;
inFile.ignore();
getline(inFile, temp.name);
cout<<"ID:  "<<temp.sensorID<<" ... "<<temp.name<<" .. ";

for(int i=0; i<MAX; i++)
{
    if(strcmp(temp.sensorID,sensors[i].sensorID)==0)
    {
        sensors[i].name=temp.name;
        matched = true;
cout<<"--matched-- i = "<<i<<"   "<<endl;
    }

}
    if(matched == false)
    {
cout<<"--not matched-- first empty = "<<firstEmpty<<"   "<<endl;
    strcpy(sensors[firstEmpty].sensorID, temp.sensorID);
    sensors[firstEmpty].name=temp.name;
    firstEmpty++;
    }

}

if(firstEmpty>=MAX){cout<<"*****WARNING - MAX sensors exceeded *****"<<endl;}

    inFile.close();
    return true;
}

/////////////////////////////NAME()////////////////////
void Name() //for naming the devicesx
{
    int choice = -1;
    while(choice<1)
    {
    //
    system("clear");
    List();
    while(choice < 0 || (choice >= MAX && (choice != 888 && choice != 999)))
    {
    cout<<"Enter line number to edit name, 888 to delete a device or 999 to save and exit"<<endl;
    cin>>choice;
    if(cin.fail() ) {cin.clear(); choice =-1;}
    cin.ignore(50, '\n');

  //  continue;
    }

    if (choice == 888){Delete_Device();choice=-1; continue;}
    if (choice==999){return;} //add return 0 here later


    cout<<"Enter new name for sensor "<<sensors[choice].sensorID;
    cout<<endl;
    getline(cin, sensors[choice].name);


    choice = -1;
    }

}
///////////////////Delete_Device()////////////////
void Delete_Device()
{
int choice = -1;
    while (choice<0 || choice > MAX)
    {
    List();
    cout<<"Select a line item to delete"<<endl;
    cin>>choice;
    if(cin.fail()){cin.clear(); cin.ignore(50, '\n');choice =-1;}
    }
    sensors[choice].sensorID[0]=' ';
    sensors[choice].sensorID[1]='\0';
    sensors[choice].name="I have no name";
    sensors[choice].isOnline=false;

}
/////////////////////////Save()////////////////////
void Save()
{
ofstream outFile;
outFile.open(MYPATH.c_str());

for (int i=0; i<firstEmpty; i++)
{
    outFile<<"#####"<<endl
    <<sensors[i].sensorID<<endl
    <<sensors[i].name<<endl;
}
outFile.close();
cout<<"saving and exiting"<<endl;
}





